## 2023.3.2

- Fix compile error on pzemdc.h [esphome#4583](https://github.com/esphome/esphome/pull/4583) by [@KG3RK3N](https://github.com/KG3RK3N)
- Swap curly brackets for round on LockGuard [esphome#4610](https://github.com/esphome/esphome/pull/4610) by [@jesserockz](https://github.com/jesserockz)
- Fix animation resizing [esphome#4608](https://github.com/esphome/esphome/pull/4608) by [@guillempages](https://github.com/guillempages)
- SX1509 minimum loop period (fixes esphome/issues#4325) [esphome#4613](https://github.com/esphome/esphome/pull/4613) by [@tracestep](https://github.com/tracestep)

